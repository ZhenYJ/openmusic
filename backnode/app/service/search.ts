import fetch from 'node-fetch';
const BaseService = require('./BaseService')

export default class Search extends BaseService {
  // 关键字搜索
  async searchKey (key) {
    return this.commonRequest(`http://www.kuwo.cn/api/www/search/searchKey?key=${key}&httpsStatus=1`)
  }

  // 单曲搜索
  async searchMusicBykeyWord ({ key, pn, rn }) {
    const response = await fetch(`https://search.kuwo.cn/r.s?client=kt&all=${key}&pn=${pn-1}&rn=${rn}&uid=794762570&ver=kwplayer_ar_9.2.2.1&vipver=1&show_copyright_off=1&newver=1&ft=music&cluster=0&strategy=2012&encoding=utf8&rformat=json&vermerge=1&mobi=1&issubtitle=1`);
    const data = await response.json();
    let result: any = {
      data: {
        list: []
      }
    }
    let list: any = [];
    let Li = data.abslist;
    for(let i=0;i<Li.length;i++){
      let picweb = '';
      if(Li[i]['web_albumpic_short'] === ''){
        picweb = 'https://img1.kuwo.cn/star/starheads/' + Li[i]['web_artistpic_short'].replace("120", "500");
      } else {
        picweb = 'https://img4.kuwo.cn/star/albumcover/' + Li[i]['web_albumpic_short'].replace("120", "500");
      }
      let PIC = picweb;
      let msg = {
        'pic': PIC,
        'name': Li[i]['SONGNAME'],
        'artist': Li[i]['ARTIST'],
        'rid': Li[i]['MUSICRID'].replace("MUSIC_", "")
      }
      list.push(msg);
    }
    result.data.list = list;
    return result;
  }

  // 专辑搜索
  async searchAlbumBykeyWord ({ key, pn, rn }) {
    return this.commonRequest(`http://www.kuwo.cn/api/www/search/searchAlbumBykeyWord?key=${key}&pn=${pn}&rn=${rn}&httpsStatus=1`)
  }

  // mv 搜索
  async searchMvBykeyWord ({ key, pn, rn }) {
    return this.commonRequest(`http://www.kuwo.cn/api/www/search/searchMvBykeyWord?key=${key}&pn=${pn}&rn=${rn}&httpsStatus=1`)
  }

  // 歌单搜索
  async searchPlayListBykeyWord ({ key, pn, rn }) {
    return this.commonRequest(`http://www.kuwo.cn/api/www/search/searchPlayListBykeyWord?key=${key}&pn=${pn}&rn=${rn}&httpsStatus=1`)
  }

  // 歌手搜索
  async searchArtistBykeyWord ({ key, pn, rn }) {
    return this.commonRequest(`http://www.kuwo.cn/api/www/search/searchArtistBykeyWord?key=${key}&pn=${pn}&rn=${rn}&httpsStatus=1`)
  }
}
